<?php

namespace kradwhite\collection;

/**
 * Description of CollectionNotFoundException
 *
 * @author Aleksandrov Artem
 */
class CollectionNotFoundException extends \Exception
{
}
