<?php

namespace kradwhite\collection;

use kradwhite\collection\Collectionable;

/**
 * Description of PublicCollection
 *
 * @author Aleksandrov Artem
 */
class PublicCollection implements Collectionable
{
	/**
	 *
	 * @var array[object]
	 */
	private $data;
	
	/**
	 * 
	 * @param array $data
	 */
	public function __construct(array $data = [])
	{
		$this->data = $data;
	}
	
	/**
	 * 
	 * @param array $field
	 * @return PublicCollection
	 */
	public function filter(array $field) : Collectionable
	{
		$name = key($field);
		$value = $field[$name];
		$result = is_array($value) ? $this->filterArray($name, $value) : $this->filterNotArray($name, $value);
		return new PublicCollection($result);
	}
	
	/**
	 * 
	 * @param array $params
	 * @return PublicCollection
	 */
	public function filters(array $params) : Collectionable
	{
		$result = [];
		$this->prepareFilters($params);
		foreach($this->data as &$model){
			$equal = true;
			foreach($params as $name => &$param){
				if(!in_array($model->$name, $param)){
					$equal = false;
					break;
				}
			}
			if($equal){
				$result[] = $model;
			}
		}
		return new PublicCollection($result);
	}
	
	/**
	 * 
	 * @param string|int $key
	 * @return array
	 */
	public function value($key) : array
	{
		return array_column($this->data, $key);
	}
	
	/**
	 * 
	 * @param array $names
	 * @return array
	 */
	public function values(array $names) : array
	{
		$result = [];
		foreach($this->data as $i => &$model){
			foreach($names as &$name){
				$result[$i][$name] = $model->$name;
			}
		}
		return $result;
	}

	/**
	 * 
	 * @return int
	 */
	public function count()
	{
		return count($this->data);
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getData() : array
	{
		return $this->data;
	}
	
	/**
	 * 
	 * @param object $element
	 * @param string|int $key
	 * @return boolean
	 */
	public function add($element, $key = NULL) : bool
	{
		if(array_key_exists($key, $this->data)){
			return false;
		}
		$key ? $this->data[$key] = $element : $this->data[] = $element;
		return true;
	}
	
	/**
	 * 
	 * @param string|int $key
	 * @return object
	 */
	public function remove($key)
	{
		$model = NULL;
		if(array_key_exists($key, $this->data)){
			$model = $this->data[$key];
			unset($this->data[$key]);
		}
		return $model;
	}
	
	/**
	 * 
	 * @param string|int $key
	 * @return object
	 */
	public function findByKey($key)
	{
		return array_key_exists($key, $this->data) ? $this->data[$key] : NULL;
	}
	
	/**
	 * 
	 * @param array $params
	 * @return object
	 */
	public function findByContent(array $params)
	{
		foreach($this->data as &$model){
			$equal = true;
			foreach($params as $name => &$param){
				if($model->$name != $param){
					$equal = false;
					break;
				}
			}
			if($equal){
				return $model;
			}
		}
	}
	
	/**
	 * 
	 * @param string|int $name
	 * @return int
	 */
	public function groupBy($name) : int
	{
		$result = [];
		foreach($this->data as $model){
            if(!isset($result[$model->$name])){
                $result[$model->$name] = $model;
            }
		}
		$this->data = $result;
		return count($this->data);
	}
	
	public function toSimpleKey()
	{
		$result = [];
		foreach($this->data as $model){
			$result[] = $model;
		}
		$this->data = $result;
	}
	
	/**
	 * 
	 * @param string $name
	 * @return mixed
	 */
	public function max(string $name)
	{
		if(empty($this->data)){
			return NULL;
		}
		$result = $this->data[key($this->data)]->$name;
		foreach($this->data as &$model){
			if($result < $model->$name){
				$result = $model->$name;
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @param string $name
	 * @return mixed
	 */
	public function min(string $name)
	{
		if(empty($this->data)){
			return NULL;
		}
		$result = $this->data[key($this->data)]->$name;
		foreach($this->data as &$model){
			if($result > $model->$name){
				$result = $model->$name;
			}
		}
		return $result;
	}

	public function &iterate()
	{
		foreach($this->data as $key => &$value){
			yield $key => $value;
		}
	}
	
	/**
	 * 
	 * @param int|string $name
	 * @param mixed $value
	 * @return array
	 */
	private function filterNotArray($name, $value) : array
	{
		$result = [];
		foreach ($this->data as &$model){
			if($model->$name == $value){
				$result[] = $model;
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @param int|string $name
	 * @param array $values
	 * @return array
	 */
	private function filterArray($name, array &$values) : array
	{
		$result = [];
		foreach($this->data as &$model){
			if(in_array($model->$name, $values)){
				$result[] = $model;
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @param array $filters
	 */
	private function prepareFilters(array &$filters)
	{
		foreach ($filters as $name => &$filter){
			if(!is_array($filter)){
				$filters[$name] = [$filter];
			}
		}
	}
}