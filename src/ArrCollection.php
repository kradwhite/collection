<?php

namespace kradwhite\collection;

use kradwhite\collection\Collectionable;

/**
 * Description of ArrCollection
 *
 * @author Aleksandrov Artem
 */
class ArrCollection implements Collectionable
{
	/**
	 *
	 * @var array
	 */
	private $data;
	
	/**
	 * 
	 * @param array $data
	 */
	public function __construct(array $data = [])
	{
		reset($data);
		$this->data = $data;
	}
	
	/**
	 * 
	 * @param array $field
	 * @return ArrCollection
	 */
	public function filter(array $field) : Collectionable
	{
		$key = key($field);
		$value = $field[$key];
		$result = is_array($value) ? $this->filterArray($key, $value) : $this->filterNotArray($key, $value);
		return new ArrCollection($result);
	}
	
	/**
	 * 
	 * @param array $fields
	 * @return ArrCollection
	 */
	public function filters(array $fields) : Collectionable
	{
		$result = [];
		$this->prepareFilters($fields);
		foreach($this->data as &$one){
			$equal = true;
			foreach($fields as $key => &$field){
				if(!in_array($one[$key], $field)){
					$equal = false;
					break;
				}
			}
			if($equal){
				$result[] = $one;
			}
		}
		return new ArrCollection($result);
	}
	
	/**
	 * 
	 * @param string|int $key
	 * @return array
	 */
	public function value($key) : array
	{
		return array_column($this->data, $key);
	}
	
	/**
	 * 
	 * @param array $keys
	 * @return array
	 */
	public function values(array $keys) : array
	{
		$result = [];
		foreach($this->data as $key => &$one){
			foreach($keys as &$oneKey){
				$result[$key][$oneKey] = $one[$oneKey];
			}
		}
		return $result;
	}

	/**
	 * 
	 * @return int
	 */
	public function count()
	{
		return count($this->data);
	}
	
	/**
	 * 
	 * @param array $element
	 * @param string|int $key
	 * @return boolean
	 */
	public function add($element, $key = NULL) : bool
	{
		if(array_key_exists($key, $this->data)){
			return false;
		}
		$key ? $this->data[$key] = $element : $this->data[] = $element;
		return true;
	}
	
	/**
	 * 
	 * @param string|int $key
	 * @return array
	 */
	public function remove($key)
	{
		$model = NULL;
		if(array_key_exists($key, $this->data)){
			$model = $this->data[$key];
			unset($this->data[$key]);
		}
		return $model;
	}
	
	/**
	 * 
	 * @param string|int $key
	 * @return array
	 */
	public function findByKey($key)
	{
		return array_key_exists($key, $this->data) ? $this->data[$key] : NULL;
	}
	
	/**
	 * 
	 * @param array $params
	 * @return object
	 */
	public function findByContent(array $params)
	{
		foreach($this->data as &$one){
			$equal = true;
			foreach($params as $name => &$param){
				if($one[$name] != $param){
					$equal = false;
					break;
				}
			}
			if($equal){
				return $one;
			}
		}
	}
	
	/**
	 * 
	 * @param string|int $name
	 * @return int
	 */
	public function groupBy($name) : int
	{
		$result = [];
		foreach($this->data as $one){
		    if(!isset($result[$one[$name]])) {
                $result[$one[$name]] = $one;
            }
		}
		$this->data = $result;
		return count($this->data);
	}
	
	public function toSimpleKey()
	{
		$result = [];
		foreach($this->data as $one){
			$result[] = $one;
		}
		$this->data = $result;
	}
	
		
	/**
	 * 
	 * @param string $name
	 * @return mixed
	 */
	public function max(string $name)
	{
		if(empty($this->data)){
			return NULL;
		}
		$result = $this->data[key($this->data)][$name];
		foreach($this->data as &$one){
			if($result < $one[$name]){
				$result = $one[$name];
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @param string $name
	 * @return mixed
	 */
	public function min(string $name)
	{
		if(empty($this->data)){
			return NULL;
		}
		$result = $this->data[key($this->data)][$name];
		foreach($this->data as &$one){
			if($result > $one[$name]){
				$result = $one[$name];
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getData() : array
	{
		return $this->data;
	}
	
	public function &iterate()
	{
		foreach($this->data as $key => &$value){
			yield $key => $value;
		}
	}
	
	/**
	 * 
	 * @param int|string $key
	 * @param mixed $value
	 * @return array
	 */
	private function filterNotArray($key, $value) : array
	{
		$result = [];
		foreach ($this->data as &$one){
			if($one[$key] == $value){
				$result[] = $one;
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @param int|string $key
	 * @param array $values
	 * @return array
	 */
	private function filterArray($key, array $values) : array
	{
		$result = [];
		foreach($this->data as &$one){
			if(in_array($one[$key], $values)){
				$result[] = $one;
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @param array $filters
	 */
	private function prepareFilters(array &$filters)
	{
		foreach ($filters as $key => &$filter){
			if(!is_array($filter)){
				$filters[$key] = [$filter];
			}
		}
	}
}
