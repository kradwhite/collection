<?php

namespace kradwhite\collection;

use kradwhite\collection\Collectionable;
use kradwhite\collection\PublicCollection;
use kradwhite\collection\PrivateCollection;
use kradwhite\collection\ArrCollection;
use kradwhite\collection\CollectionNotFoundException;

/**
 * Description of Collection
 *
 * @author Aleksandrov Artem
 */
class Collection
{
	const PUB = 1;
	const PRI = 2;
	const ARR = 3;
	
	public static function build(int $key, array $data) : Collectionable
	{
		if($key == Collection::PRI){
			return new PrivateCollection($data);
		}else if($key == Collection::PUB){
			return new PublicCollection($data);
		}else if($key == Collection::ARR){
			return new ArrCollection($data);
		}else{
			throw new CollectionNotFoundException("Unknown value of the key for choose collection.");
		}
	}
}
