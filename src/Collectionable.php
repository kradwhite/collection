<?php

namespace kradwhite\collection;

/**
 *
 * @author Aleksandrov Artem
 */
interface Collectionable extends \Countable
{
	/**
	 * 
	 * @param array $data
	 */
	public function __construct(array $data);
	
	/**
	 *
	 * @param array $field Example: ['id' => 23] or ['id' => [34, 58]]
	 * @return Collectionable
	 */
	public function filter(array $field) : Collectionable;
	
	/**
	 * 
	 * @param array $fields Example: ['id => 23, 'buz' => 'foo']
	 * @return Collectionable
	 */
	public function filters(array $fields) : Collectionable;
	
	/**
	 * 
	 * @param int|string $key
	 * @return array
	 */
	public function value($key) : array;
	
	/**
	 * 
	 * @param array $keys
	 * @return array
	 */
	public function values(array $keys) : array;
	
	/**
	 * @return array
	 */
	public function getData() : array;
	
	/**
	 * 
	 * @param array|object $element
	 * @param string|int $key
	 * @return bool
	 */
	public function add($element, $key = NULL) : bool;
	
	/**
	 * 
	 * @param string|int $key
	 * @return array|object
	 */
	public function remove($key);
	
	/**
	 * 
	 * @param string|int $key
	 * @return array|object
	 */
	public function findByKey($key);
	
	/**
	 * 
	 * @param array $params
	 * @return array|object
	 */
	public function findByContent(array $params);
	
	/**
	 * 
	 * @param string|int $name
	 * @return int
	 */
	public function groupBy($name) : int;
	
	public function toSimpleKey();
	
	/**
	 * 
	 * @param string $name
	 * @return mixed
	 */
	public function max(string $name);
	
	/**
	 * 
	 * @param string $name
	 * @return mixed
	 */
	public function min(string $name);
	
	public function &iterate();
}
