<?php

use kradwhite\collection\PublicCollection;

class ObjTest
{
	public $id;
	public $foo;
	public $buz;
	
	public function __construct($id, $foo, $buz)
	{
		$this->id = $id;
		$this->foo = $foo;
		$this->buz = $buz;
	}
}

class PublicCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testInit()
    {
		new PublicCollection([]);
		new PublicCollection([new ObjTest(1, 2, 3), new ObjTest(1, 2, 3)]);
    }
	
	public function testFilterNotArray()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filter(['id' => 40])->getData();
		$this->assertEquals(count($result), 1);
		$this->assertEquals($result[0]->id, 40);
		$this->assertEquals($result[0]->foo, 'foo3');
		$this->assertEquals($result[0]->buz, 'buz3');
	}
	
	public function testFilterNotArrayEmpty()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filter(['id' => 54]);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testFilterArray()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filter(['id' => [25, 40]])->getData();
		$this->assertEquals($result[0]->id, 25);
		$this->assertEquals($result[0]->foo, 'foo1');
		$this->assertEquals($result[0]->buz, 'buz1');
		$this->assertEquals($result[1]->id, 40);
		$this->assertEquals($result[1]->foo, 'foo3');
		$this->assertEquals($result[1]->buz, 'buz3');
	}
	
	public function testFilterArrayEmpty()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filter(['id' => [1, 45]]);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testFiltersNotArray()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filters(['id' => 40, 'foo' => 'foo3'])->getData();
		$this->assertEquals($result[0]->id, 40);
		$this->assertEquals($result[0]->foo, 'foo3');
		$this->assertEquals($result[0]->buz, 'buz3');
	}
	
	public function testFiltersNotArrayEmpty()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filters(['id' => 40, 'foo' => 'foo2']);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testFiltersArray()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filters(['id' => [25, 40], 'foo' => ['foo1', 'foo3']])->getData();
		$this->assertEquals($result[0]->id, 25);
		$this->assertEquals($result[0]->foo, 'foo1');
		$this->assertEquals($result[0]->buz, 'buz1');
		$this->assertEquals($result[1]->id, 40);
		$this->assertEquals($result[1]->foo, 'foo3');
		$this->assertEquals($result[1]->buz, 'buz3');
	}
	
	public function testFiltersArray2()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filters(['id' => [25, 40], 'foo' => ['foo1', 'foo2']])->getData();
		$this->assertEquals($result[0]->id, 25);
		$this->assertEquals($result[0]->foo, 'foo1');
		$this->assertEquals($result[0]->buz, 'buz1');
	}
	
	public function testFiltersArrayEmpty()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->filters(['id' => [25, 40], 'foo' => ['wrong', 'str']]);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testValue()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->value("id");
		$this->assertEquals($result, [25, 27, 40]);
	}
	
	public function testValueEmpty()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->value("wrong");
		$this->assertEquals($result, []);
	}
	
	public function testValues()
	{
		$model = new PublicCollection($this->getTestData1());
		$result = $model->values(['id', 'foo', 'buz']);
		$this->assertEquals($result, [
			['id' => 25, 'foo' => 'foo1', 'buz' => 'buz1'],
			['id' => 27, 'foo' => 'foo2', 'buz' => 'buz2'],
			['id' => 40, 'foo' => 'foo3', 'buz' => 'buz3'],
		]);
	}
	
	public function testCount()
	{
		$model = new PublicCollection($this->getTestData1());
		$this->assertEquals($model->count(), count($this->getTestData1()));
		$model2 = new PublicCollection([]);
		$this->assertEquals($model2->count(), 0);
	}
	
	public function testLoopForeach()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		foreach($model as $key => &$value){
			$this->assertEquals(key($data), $key);
			$this->assertEquals($data[key($data)], $value);
			next($data);
		}
	}
	
	public function testAddFail()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$before = $model->count();
		$result = $model->add(new ObjTest(50, 'foo4', 'buz4'), 2);
		$after = $model->count();
		$this->assertFalse($result);
		$this->assertEquals($before, $after);
	}
	
	public function testAddSuccess()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$before = $model->count();
		$result = $model->add(new ObjTest(50, 'foo4', 'buz4'));
		$after = $model->count();
		$this->assertTrue($result);
		$this->assertEquals($before + 1, $after);
	}
	
	public function testRemoveFail()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$before = $model->count();
		$result = $model->remove(4);
		$after = $model->count();
		$this->assertNull($result);
		$this->assertEquals($before, $after);
	}
	
	public function testRemoveSuccess()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$before = $model->count();
		$result = $model->remove(2);
		$after = $model->count();
		$this->assertInstanceOf('ObjTest', $result);
		$this->assertEquals($before - 1, $after);
	}
	
	public function testFindByKeyFail()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$result = $model->findByKey(4);
		$this->assertNull($result);
	}
	
	public function testFindByKeySuccess()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$result = $model->findByKey(2);
		$this->assertInstanceOf('\ObjTest', $result);
	}
	
	public function testFindByContentFail()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$result = $model->findByContent(['id' => 49]);
		$this->assertNull($result);
	}
	
	public function testFindByContentSuccess()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$result = $model->findByContent(['id' => 27]);
		$this->assertInstanceOf('\ObjTest', $result);
	}
	
	public function testGroupBy()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$count = $model->count();
		$result = $model->groupBy('id');
		$this->assertEquals($count, $result);
		$this->assertNotNull($model->findByKey(25));
		$this->assertNotNull($model->findByKey(27));
		$this->assertNotNull($model->findByKey(40));
	}
	
	public function testToSimpleKey()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$model->groupBy('id');
		$model->toSimpleKey();
		$this->assertNotNull($model->findByKey(0));
		$this->assertNotNull($model->findByKey(1));
		$this->assertNotNull($model->findByKey(2));
	}
	
	public function testMax()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$this->assertEquals($model->max('id'), 40);
		$this->assertEquals($model->max('foo'), 'foo3');
	}
	
	public function testMin()
	{
		$data = $this->getTestData1();
		$model = new PublicCollection($data);
		$this->assertEquals($model->min('id'), 25);
		$this->assertEquals($model->min('foo'), 'foo1');
	}
	
	public function testMaxEmpty()
	{
		$model = new PublicCollection([]);
		$this->assertNull($model->max('id'));
	}
	
	public function testMinEmpty()
	{
		$model = new PublicCollection([]);
		$this->assertNull($model->min('id'));
	}
	
	private function getTestData1()
	{
		return [
			new ObjTest(25, 'foo1', 'buz1'),
			new ObjTest(27, 'foo2', 'buz2'),
			new ObjTest(40, 'foo3', 'buz3')
		];
	}
}