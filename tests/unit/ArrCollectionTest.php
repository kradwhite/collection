<?php

use kradwhite\collection\ArrCollection;

class ArrCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testInit()
    {
		new ArrCollection([]);
		new ArrCollection([0, 1]);
    }
	
	public function testFilterNotArray()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filter(['k1' => 40]);
		$this->assertEquals($result->getData(), [['k1' => 40, 'k2' => 'str3']]);
	}
	
	public function testFilterNotArrayEmpty()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filter(['k1' => 54]);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testFilterArray()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filter(['k1' => [25, 40]]);
		$this->assertEquals($result->getData(), [['k1' => 25, 'k2' => 'str1'], ['k1' => 40, 'k2' => 'str3']]);
	}
	
	public function testFilterArrayEmpty()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filter(['k1' => [1, 45]]);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testFiltersNotArray()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filters(['k1' => 40, 'k2' => 'str3']);
		$this->assertEquals($result->getData(), [['k1' => 40, 'k2' => 'str3']]);
	}
	
	public function testFiltersNotArrayEmpty()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filters(['k1' => 40, 'k2' => 'str2']);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testFiltersArray()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filters(['k1' => [25, 40], 'k2' => ['str1', 'str3']]);
		$this->assertEquals($result->getData(), [['k1' => 25, 'k2' => 'str1'], ['k1' => 40, 'k2' => 'str3']]);
	}
	
	public function testFiltersArray2()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filters(['k1' => [25, 40], 'k2' => ['str1', 'str2']]);
		$this->assertEquals($result->getData(), [['k1' => 25, 'k2' => 'str1']]);
	}
	
	public function testFiltersArrayEmpty()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->filters(['k1' => [25, 40], 'k2' => ['wrong', 'str']]);
		$this->assertEquals($result->getData(), []);
	}
	
	public function testValue()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->value("k1");
		$this->assertEquals($result, [25, 27, 40]);
	}
	
	public function testValueEmpty()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->value("wrong");
		$this->assertEquals($result, []);
	}
	
	public function testValues()
	{
		$model = new ArrCollection($this->getTestData1());
		$result = $model->values(['k1', 'k2']);
		$this->assertEquals($result, $this->getTestData1());
	}
	
	public function testCount()
	{
		$model = new ArrCollection($this->getTestData1());
		$this->assertEquals($model->count(), count($this->getTestData1()));
		$model2 = new ArrCollection([]);
		$this->assertEquals($model2->count(), 0);
	}
	
	public function testLoopForeach()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		foreach($model as $key => &$value){
			$this->assertEquals(key($data), $key);
			$this->assertEquals($data[key($data)], $value);
			next($data);
		}
	}
	
	public function testAddFail()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$before = $model->count();
		$result = $model->add(['k1' => 45, 'k2' => 'str4'], 2);
		$after = $model->count();
		$this->assertFalse($result);
		$this->assertEquals($before, $after);
	}
	
	public function testAddSuccess()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$before = $model->count();
		$result = $model->add(['k1' => 45, 'k2' => 'str4']);
		$after = $model->count();
		$this->assertTrue($result);
		$this->assertEquals($before + 1, $after);
	}
	
	public function testRemoveFail()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$before = $model->count();
		$result = $model->remove(4);
		$after = $model->count();
		$this->assertNull($result);
		$this->assertEquals($before, $after);
	}
	
	public function testRemoveSuccess()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$before = $model->count();
		$result = $model->remove(2);
		$after = $model->count();
		$this->assertEquals(['k1' => 40, 'k2' => 'str3'], $result);
		$this->assertEquals($before - 1, $after);
	}
	
		public function testFindFail()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$result = $model->findByKey(4);
		$this->assertNull($result);
	}
	
	public function testFindSuccess()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$result = $model->findByKey(1);
		$this->assertEquals(['k1' => 27, 'k2' => 'str2'], $result);
	}
	
	public function testFindByContentFail()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$result = $model->findByContent(['k1' => 49]);
		$this->assertNull($result);
	}
	
	public function testFindByContentSuccess()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$result = $model->findByContent(['k1' => 27]);
		$this->assertEquals(['k1' => 27, 'k2' => 'str2'], $result);
	}
	
	public function testGroupBy()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$count = $model->count();
		$result = $model->groupBy('k1');
		$this->assertEquals($count, $result);
		$this->assertNotNull($model->findByKey(25));
		$this->assertNotNull($model->findByKey(27));
		$this->assertNotNull($model->findByKey(40));
	}
	
	public function testToSimpleKey()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$model->groupBy('k1');
		$model->toSimpleKey();
		$this->assertNotNull($model->findByKey(0));
		$this->assertNotNull($model->findByKey(1));
		$this->assertNotNull($model->findByKey(2));
	}
		
	public function testMax()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$this->assertEquals($model->max('k1'), 40);
		$this->assertEquals($model->max('k2'), 'str3');
	}
	
	public function testMin()
	{
		$data = $this->getTestData1();
		$model = new ArrCollection($data);
		$this->assertEquals($model->min('k1'), 25);
		$this->assertEquals($model->min('k2'), 'str1');
	}
	
	public function testMaxEmpty()
	{
		$model = new ArrCollection([]);
		$this->assertNull($model->max('id'));
	}
	
	public function testMinEmpty()
	{
		$model = new ArrCollection([]);
		$this->assertNull($model->min('id'));
	}
	
	
	private function getTestData1()
	{
		return [
			['k1' => 25, 'k2' => 'str1'],
			['k1' => 27, 'k2' => 'str2'],
			['k1' => 40, 'k2' => 'str3']
		];
	}
}