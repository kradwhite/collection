<?php

use kradwhite\collection\Collection;
use kradwhite\collection\CollectionNotFoundException;
use kradwhite\collection\ArrCollection;
use kradwhite\collection\PrivateCollection;
use kradwhite\collection\PublicCollection;

class CollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testBuildException()
    {
		$this->tester->expectException(CollectionNotFoundException::class, function(){
			Collection::build(2432, []);
		});
    }
	
	public function testBuildSuccess()
	{
		$model = Collection::build(Collection::PUB, []);
		$this->assertInstanceOf(PublicCollection::class, $model);
		$model2 = Collection::build(Collection::PRI, []);
		$this->assertInstanceOf(PrivateCollection::class, $model2);
		$model3 = Collection::build(Collection::ARR, []);
		$this->assertInstanceOf(ArrCollection::class, $model3);
	}
}